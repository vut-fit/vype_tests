#!/bin/bash

# USAGE:
# run all unit tests
# ./run_tests.sh unit

# test return codes
# ./run_tests.sh e2e return
# test outputs
# ./run_tests.sh e2e output
# default is bothe
# ./run_tests.sh e2e

# defaults
selected="$1"
if [ "$selected" = "" ]
    then
    selected="all"
fi

unitRegex="$2"
if [ "$unitRegex" = "" ]
    then
    unitRegex="*"
fi

if [ "$selected" = "unit" ] || [ "$selected" = "all" ]
    then
    python -m unittest discover -s tests/  -p "$unitRegex"
fi

[ "$selected" = "e2e" ] || [ "$selected" = "all" ] || exit

program=./vype

# test vykoname pre vsetky subory v ciselne oznacenych slozkach
# ciselne oznacenie UDAVA navratovu hodnotu, ktore maju subory
# v danej slozke vracat
tests_folder=tests/return_codes
failed=0
[ "$2" != 'output' ] && for i in $( ls $tests_folder ); do
    # pre vsetky adresare, ktore maju celociselny nazov
    # if [[ -d "$i" && -z "${i##[0-9]*}" ]]
    #    then
       # spustime nas program pre vsetky subory v tomto adreseri
       for f in $( ls "${tests_folder}/$i" ); do
           "$program" "${tests_folder}/$i/$f" 2> /dev/null
           retCode=`echo $?`
           #  ak testy NEDOPADLI spravne vypis chybu
           if [ "$retCode" != "$i" ]
              then
              (( failed += 1 ))
              echo "Error!!!!! $i/$f: PROGRAM Returned: $retCode"
           fi
       done
    # fi
done

[ "$2" != 'output' ] && echo "Return code tests failed: $failed"

[ "$2" != "return" ] || exit


cd tests/codasip/
# fill in numbes of tets to run (separted with comma)
for c_file in ../resources/*.c
  do
  echo
  # strip sufix and prefix
  filename=${c_file%.c}
  filename=${filename#../resources/}
  # input .c -> .asm
  ../../vype $c_file ${filename}.asm
  # .asm, stdin -> stdout, stderr
  # input is in resources_input folder
  if [ ! -e "../resources_input/${filename}.in" ] ; then
    touch "../resources_input/${filename}.in"
  fi
  ./make_and_run.sh $filename > ../tmp_output/pom_${filename}.out
  head -n -27 ../tmp_output/pom_${filename}.out | sed 's/--- Simulation.*$//' > ../tmp_output/${filename}.out
  #head -n -28 ../tmp_output/pom_${filename}.out  > ../tmp_output/${filename}.out

  # compare expected with actual
  (diff ../resources_output/${filename}.out ../tmp_output/${filename}.out  && echo "PASS: ${filename}" )|| echo "ERROR: ${filename}.c"
done
cd ../..;
