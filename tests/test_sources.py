import unittest
import os
from os import listdir
from parser_module import Parser


class TestSourcesTestCase(unittest.TestCase):

    def test_all_sources(self):
        dir = os.path.dirname(__file__) + '/resources'
        sources = [dir + '/' + f for f in listdir(dir) if os.path.splitext(f)[1] == '.c']
        for source in sources:
            print source
            with open(source, "r") as f:
                data = f.read()
                self.parse(data)

    def test_source(self):
        dir = os.path.dirname(__file__) + '/resources'
        sources = [f for f in listdir(dir) if os.path.splitext(f)[1] == '.c']
        for source in sources:
            if source == "2-recursive-factorial.c":
                with open(dir + '/' + source, "r") as f:
                    data = f.read()
                    self.parse(data)
                    pass

    # shortcut
    def parse(self, i):
        parser = Parser(debug=0)
        return parser.parse(i)