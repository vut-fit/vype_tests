import unittest
import os

from parser_module import Parser
from compiler_exceptions import SyntaxErrorException

class TestParserTestCase(unittest.TestCase):

  def test_syntax_error(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse("void m ain() {};")

  def test_main(self):
    self.parse("int main(void) {}")

  # function definition
  ###############################################
  def test_func_definition(self):
    self.parse("int main(void) {}")

  def test_func_multiple_definitions(self):
    self.parse("void myFunc1(void) {} void myFunc2(void) {}")

  # TODO(gb): this belongs to tests for semantic analyzer
  # def test_func_multiple_mains(self):
    # with self.assertRaises(SemanticErrorException):
      # self.parse("int main(void) {} int main(void) {}")

  # function declaration
  ###############################################
  def test_func_declaration(self):
    self.parse("int main(void) {} void myFunc(void) {}")

  def test_func_multiple_declarations(self):
    self.parse("int main(void) {} void myFunc1(void) {} void myFunc2(void) {}")

  def test_func_multiple_definitions(self):
    self.parse("int main(void) {} void myFunc1(void){} void myFunc2(void){}")

  # paramaters
  ###############################################
  def test_parameters(self):
    self.parse("int main(void) {} char f(int p1) {}")
    self.parse("int main(void) {} int f(int p1, int p2) {}")
    self.parse("int main(void) {} string f(int p1, char p2, string p3) {}")

  def test_invalid_data_parameter(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse('char f(int p1, void p2)')

  def test_invalid_parameters(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse('char f(int p1, void)')

  def test_void_is_not_in_params(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse("void myFunc(void, int, string) {}")
      self.parse("void myFunc(string, void, string) {}")
      self.parse("void myFunc(char, int, int, void) {}")

  # data paramaters
  ###############################################
  def test_data_params(self):
    self.parse("int main(void) {} void myFunc(int a) {}")
    self.parse("int main(void) {} void myFunc(char a, int b) {}")
    self.parse("int main(void) {} void myFunc(char a, int b, string c) {}")

  def test_void_is_not_in_data_params(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse("void myFunc(void, int, string);")
      self.parse("void myFunc(string, void, string);")
      self.parse("void myFunc(char, int, int, void);")

  # declaration of variables
  ###############################################
  def test_declaration(self):
    self.parse('int main(void){ int a; }')
    self.parse('int main(void){ char a, b; }')
    self.parse('int main(void){ string a, b, c; }')

  def test_void_is_not_data_type(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse('int main(void){ void a; }')

  # # assignment
  # ###############################################
  def test_assignment(self):
    self.parse('int main(void){int a, b, c, d; a = b; }')

  # if-else
  ###############################################
  def test_if_else(self):
    self.parse('int main(void){int a; if (a) { } else {}  }')

  # while
  ###############################################
  def test_while(self):
    self.parse('int main(void){int a; while (a) { } }')

  # function call
  ###############################################
  def test_function_call(self):
    self.parse('int fun(void) {} int main(void){ fun(); }')
    self.parse('int fun(int a) {} int main(void){int a; fun(a); }')
    self.parse('int fun(int a, int b) {} int main(void){int a,b; fun(a, b); }')
    self.parse('int fun(int a, int b, int c){} int main(void){int a,b,c; fun(a, b, c); }')

  # return
  ###############################################
  def test_return(self):
    self.parse('int main(void){int a; return a; }')

  def test_return_not_expression_list(self):
    with self.assertRaises(SyntaxErrorException):
      self.parse('int main(void) {int a, b; return a, b; }')

  # expressions - in order of ascending priority (lowest -> highest)
  ###############################################
  def test_logical_expressions(self):
    self.parse('int main(void){int a, b, r; r = a || b; }')
    self.parse('int main(void){int a, b, r; r = a && b; }')

  def test_relational_expressions(self):
    self.parse('int main(void){int a, b,r; r = a < b; }')
    self.parse('int main(void){int a, b,r; r = a > b; }')
    self.parse('int main(void){int a, b,r; r = a <= b; }')
    self.parse('int main(void){int a, b,r; r = a >= b; }')

  def test_equality_expressions(self):
    self.parse('int main(void){int a, b,r; r = a == b; }')
    self.parse('int main(void){int a, b,r; r = a != b; }')

  def test_arithmetic_expressions_consts(self):
    self.parse('int main(void){int r; r = 1 + 2; }')
    self.parse('int main(void){int r; r = 3 - 4; }')
    self.parse('int main(void){int r; r = 5 % 6; }')
    self.parse('int main(void){int r; r = 7 * 8; }')
    self.parse('int main(void){int r; r = 9 / 1; }')

  def test_arithmetic_expressions(self):
    self.parse('int main(void){int a; int b, r; r = a + b; }')
    self.parse('int main(void){int a; int b, r; r = a - b; }')
    self.parse('int main(void){int a; int b, r; r = a % b; }')
    self.parse('int main(void){int a; int b, r; r = a * b; }')
    self.parse('int main(void){int a; int b, r; r = a / b; }')

  def test_not_expression(self):
    self.parse('int main(void){int b,r; r = !b; }')

  def test_casting_expression(self):
    self.parse('int main(void){int b, r; r = (int)b; }')
    self.parse('int main(void){char b, r; r = (char)b; }')

  def test_function_call_expression(self):
    self.parse('int b(void) {} int main(void){int r; r = b(); }')

  def test_parantheses(self):
    self.parse('int main(void){int a, b, r; r = (a + b); }')

  def test_compound_expressions(self):
    self.parse('int main(void){int a, b, d, r; r = a / b + d; }')
    self.parse('int main(void){int a, b, d, r; r = a * b + d; }')
    self.parse('int main(void){int a, b, c, r; r = a * b + c; }')
    self.parse('int main(void){int a, b, c, r; r = a / b - c; }')

  def test_expressions_inside_statements(self):
    self.parse('int main(void){int a, b, c; if (a + b > c) { } else {}  }')
    self.parse('int main(void){int a, b, c; while (a >= b) { } }')
    self.parse('int fun(int a, int b, int c) {} int main(void){int a, b, c; fun(a * 5, b + 2, c % (5 / 2)); }')
    self.parse('int main(void){int a, b, c; return (int)(a * 8 * 5 + 2); }')

  #
  ###############################################
  def test_(self):
    self.parse('int main(void){ }')

  def test_str_litral(self):
    self.parse('int main(void) { string s; s = "s"; s = "s";}')
  # programs
  ###############################################
  def test_iterative_factorial(self):
    with open(os.path.dirname(__file__) + "/resources/1-iterative-factorial.c", "r") as file:
      data = file.read()

    self.parse(data)

  def test_recursive_factorial(self):
    with open(os.path.dirname(__file__) + "/resources/2-recursive-factorial.c", "r") as file:
        data = file.read()

    self.parse(data)

  def test_string_and_built_in_operations(self):
    with open(os.path.dirname(__file__) + "/resources/3-string-and-built-in-operations.c", "r") as file:
      data = file.read()

    self.parse(data)

  def test_01_expr_plus_constants(self):
    with open(os.path.dirname(__file__) + "/resources/01-expr-plus-constants.c", "r") as file:
      data = file.read()

    self.parse(data)


  ###############################################
  # shortcut
  def parse(self, input):
    parser = Parser(debug=1, )
    return parser.parse(input)

  # def tearDown(self):

if __name__ == '__main__':
  unittest.main(verbosity=2)
