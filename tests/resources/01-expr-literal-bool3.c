int main(void)
{
  int a;
  int b;
  int c;
  a = 5;
  b = 0;
  c = !1 || !0;
  print(c);
  c = a > b && a > 0;
  print(c);
  c = a == 5 || a == 0;
  print(c);
  c = !1 && !0;
  print(c);
  c = a > b && a < 0;
  print(c);
  c = a == 5 && a == 0;
  print(c);
  
}
