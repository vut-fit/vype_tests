int main(void)
{
  string a, b, c, d, x, y;
  int r;
  a = "a";
  b = "a";
  c = "";
  d = a;
  
  x = "abc";
  y = "abd";
  
  r = a == b;
  print(r);
  r = c == d;
  print(r);
  
  r = a != b;
  print(r);
  r = c != d;
  print(r);
  
  print('\n');
  
  r = a < b;
  print(r);
  r = c < d;
  print(r);
  r = x < y;
  print(r);
  
  r = b < a;
  print(r);
  r = d < c;
  print(r);
  r = y < x;
  print(r);
  
  print('\n');
  
  r = a >= b;
  print(r);
  r = c >= d;
  print(r);
  r = x >= y;
  print(r);
  
  r = b >= a;
  print(r);
  r = d >= c;
  print(r);
  r = y >= x;
  print(r);
  
  print('\n');
 
  r = a > b;
  print(r);
  r = c > d;
  print(r);
  r = x > y;
  print(r);
  
  r = b > a;
  print(r);
  r = d > c;
  print(r);
  r = y > x;
  print(r);
 
   print('\n');
   
  r = a <= b;
  print(r);
  r = c <= d;
  print(r);
  r = x <= y;
  print(r);
  
  r = b <= a;
  print(r);
  r = d <= c;
  print(r);
  r = y <= x;
  print(r);
   
}
