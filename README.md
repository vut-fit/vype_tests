VYPe tests
===================
These tests were made as a part of project “Implementation of programming language VYPe14 compiler” at FIT VUTBR in Brno.
Further details can be found in specification: https://wis.fit.vutbr.cz/FIT/st/course-files-st.php/course/VYP-IT/projects/vype2014.pdf.


USAGE
-----------------
Copy content of this repository to folder where vype compiler is available and run tests.

```
# test return codes
./run_tests.sh e2e return
# test outputs
./run_tests.sh e2e output
# default is both
./run_tests.sh e2e

# unit tests require same names, ... (as our compiler)
# They are here only for inspiration - look at the source code
# ./run_tests.sh unit
```
